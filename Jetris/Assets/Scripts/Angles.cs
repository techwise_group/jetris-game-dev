using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Angles : MonoBehaviour
{
    void Start()
    {
        Debug.Log("transform.eulerAngles: " + this.transform.eulerAngles);
        Debug.Log("transform.localEulerAngles: " + this.transform.localEulerAngles);
        Debug.Log("transform.rotation: " + this.transform.rotation);
        // Debug.Log("transform.rotation.Euler: " + this.transform.rotation.Euler);
        Debug.Log("transform.rotation.eulerAngles: " + this.transform.rotation.eulerAngles);
    }
}
